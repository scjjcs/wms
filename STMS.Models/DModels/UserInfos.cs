﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STMS.Models.DModels
{
    /// <summary>
    /// 用户实体类
    /// </summary>
    //应用自定义特性的方式    
    [Table("UserInfos")]
    [PrimaryKey("UserId",autoIncrement=true)]//这里有两个自定义特性Table、PrimaryKey
    public class UserInfos
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int UserPwd { get; set; }
        public int UserState { get; set; }
        public int IsDeleted { get; set; }



    }
}
