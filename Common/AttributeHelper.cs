﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class AttributeHelper
    {

        /// <summary>
        /// 获取映射表名
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        
        public static string GetTName(this Type type)//向Type中添加GetTName扩展方法
        {
            string tableName = string.Empty;
            //获取指定类型的所有自定义特性
            object[] attrs = type.GetCustomAttributes(false);
            foreach (var attr in attrs)
            {
                if (attr is TableAttribute)
                {
                    TableAttribute tableAttribute = attr as TableAttribute;
                    tableName = tableAttribute.TableName;
                }
            }
            if (string.IsNullOrEmpty(tableName))
            {
                tableName = type.Name;
            }
            return tableName;
        }

        /// <summary>
        /// 获取映射列名
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static string GetColName (this PropertyInfo property)
        {

            string columnName = string.Empty;
            //获取指定类型的所有自定义特性
            object[] attrs = property.GetCustomAttributes(false);
            foreach (var attr in attrs)
            {
                if (attr is ColumnAttribute)
                {
                    ColumnAttribute colAttr = attr as ColumnAttribute;
                    columnName = colAttr.ColumnName;
                }
            }
            if (string.IsNullOrEmpty(columnName))
            {
                columnName = property.Name;
            }
            return columnName;
        }

        /// <summary>
        /// 获取主键名
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetPrimary(this Type type) 
        {
            object[] attributes = type.GetCustomAttributes(false);
            foreach (var attr in attributes)
            {
                if (attr is PrimaryKeyAttribute)
                {
                    PrimaryKeyAttribute PrimaryKeyAttr = attr as PrimaryKeyAttribute;
                   return PrimaryKeyAttr.PrimaryKey;
                }
            }
            return null;
        }

        /// <summary>
        /// 判断属性是否为主键
        /// </summary>
        public static bool IsPrimary(this Type type,PropertyInfo property)
        {           
            string primaryKeyName = type.GetPrimary(); //获取主键名
            string columnName = property.GetColName();//获取指定属性的映射列名
            return (primaryKeyName==columnName);//判断是否相等
        }

        /// <summary>
        /// 判断主键是否自增
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsIncrement(this Type type) 
        {
            //获取指定类型的所有自定义特性
            object[] attributes = type.GetCustomAttributes (false);
            foreach (var attr in attributes)
            {
                if (attr is PrimaryKeyAttribute)//判断是否为主键
                {
                    PrimaryKeyAttribute primaryKeyAttr = attr as PrimaryKeyAttribute;
                    return primaryKeyAttr.autoIncrement;
                }
            }
            return false;
        }
    }
}

