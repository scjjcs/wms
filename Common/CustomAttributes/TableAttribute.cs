﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomAttributes
{
    /// <summary>
    /// 表名特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]//标志着特性应用于类
    public class TableAttribute : Attribute//继承，继承Attribute中的非私有属性
    {
        public string TableName{get;protected set;}
        public TableAttribute(string TableName)
        {
            this.TableName = TableName;
        }
    }
}
