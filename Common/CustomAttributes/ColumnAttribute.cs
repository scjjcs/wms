﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomAttributes
{
    /// <summary>
    /// 列名映射特性，列名和数据库中不一样时才派上用场。这里封装一套通用的
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]//反射技术
    public class ColumnAttribute:Attribute
    {
        public string ColumnName { get; protected set; }
        public ColumnAttribute(string colName)
        {
            this.ColumnName = colName;
        }
    }
}
