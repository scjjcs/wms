﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    
    public class DbConvert
    {
        /// <summary>
        /// 将DataRow转换成实体
        /// </summary>
        private static T DataRowToModel<T>(DataRow dr,string cols)
        {
            //创建实体对象
            T model= Activator.CreateInstance<T>();

            Type type=typeof(T);
            if (dr != null)
            {
                //获取指定列明的指定数组
                var properties = PropertyHelper.GetTypeProperties<T>(cols);
                //将第列的值赋值给对应的属性
                foreach (var p in properties)
                {
                    string colName = p.GetColName();
                    if (dr[colName] is DBNull)
                    {
                        p.SetValue(model, null);//为属性设置值
                    }
                    else
                    {
                        SetPropertyValue<T>(model, dr[colName], p);
                    }
                }
                return model;
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// 将DataTable转换成list<T>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        public static List<T> DataTableToList<T>(DataTable dt,string cols)
        {
            List<T> list = new List<T>();
            if (dt.Rows.Count>0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //将每行转换为一个对象
                    T model = DataRowToModel<T>(dr,cols);
                    list.Add(model);
                }
            }
            return list;

        }

        /// <summary>
        /// 将sqldatareader对象转换为实体（返回一条数据）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        public static T SqlDataReaderToModel<T>(SqlDataReader reader,string cols)
        {
            T model=Activator.CreateInstance<T>();
            Type type = typeof(T);
            var properties= PropertyHelper.GetTypeProperties<T>(cols);
            if (reader.Read())
            {
                foreach (var p in properties)
                {
                    string colName = p.GetColName();
                    if (reader[colName] is DBNull)
                    {
                        p.SetValue(model, null);
                    }
                    else
                    {
                        SetPropertyValue<T>(model, reader[colName], p);
                    }
                }
                return model;
            }
            else 
            {
                return default(T); 
            }
        }

        /// <summary>
        /// sqldatareader转换成list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        public static List<T>SqlDataReaderToList<T>(SqlDataReader reader,string cols)
        {
           List <T> list = new List<T>();
            Type Type = typeof(T);
            var properties= PropertyHelper.GetTypeProperties<T>(cols);
            while (reader.Read())
            {
                T model = Activator.CreateInstance<T>();
                //将指定列的值赋值给对应的属性
                foreach (var p in properties)
                {
                    string colName=p.GetColName();
                    if (reader[colName] is DBNull)
                    {
                        p.SetValue(model,null);
                    }
                    else
                    {
                        SetPropertyValue<T>(model, reader[colName],p);
                    }                    
                }
                list.Add(model);
            }
            return list;
        }

        /// <summary>
        /// 设置属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="obj"></param>
        /// <param name="property"></param>
        /// 
        //设置属性值 针对属性的数据类型为Nullable<int> int?  的情况
        private static void SetPropertyValue<T>(T model,object obj,PropertyInfo property)
        {
            if (property.PropertyType.IsGenericType&&   //判断是否为泛型类型
                property.PropertyType.GetGenericTypeDefinition().Equals(typeof  //获取泛型类型的基准类型
                (Nullable<>))) 
            { 
                property.SetValue(model, Convert.ChangeType(obj,
                    Nullable.GetUnderlyingType(property.PropertyType)));
            }
            else
            {
                property.SetValue(model,Convert.ChangeType(obj, 
                    property.PropertyType));
            }

        }









    }
}
