USE [master]
GO
/****** Object:  Database [STMSNewDBase]    Script Date: 2021/4/12 09:45:01 ******/
CREATE DATABASE [STMSNewDBase] ON  PRIMARY 
( NAME = N'STMSNewDBase', FILENAME = N'E:\Gitee项目\项目4_STMS\STMS\STMSNewDBase.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'STMSNewDBase_log', FILENAME = N'E:\Gitee项目\项目4_STMS\STMS\STMSNewDBase_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [STMSNewDBase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [STMSNewDBase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [STMSNewDBase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [STMSNewDBase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [STMSNewDBase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [STMSNewDBase] SET ARITHABORT OFF 
GO
ALTER DATABASE [STMSNewDBase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [STMSNewDBase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [STMSNewDBase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [STMSNewDBase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [STMSNewDBase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [STMSNewDBase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [STMSNewDBase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [STMSNewDBase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [STMSNewDBase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [STMSNewDBase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [STMSNewDBase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [STMSNewDBase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [STMSNewDBase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [STMSNewDBase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [STMSNewDBase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [STMSNewDBase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [STMSNewDBase] SET RECOVERY FULL 
GO
ALTER DATABASE [STMSNewDBase] SET  MULTI_USER 
GO
ALTER DATABASE [STMSNewDBase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [STMSNewDBase] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'STMSNewDBase', N'ON'
GO
USE [STMSNewDBase]
GO
/****** Object:  Table [dbo].[ProductInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductInfos](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductNo] [varchar](20) NOT NULL,
	[ProductName] [nvarchar](50) NOT NULL,
	[FitLowTemperature] [decimal](18, 2) NULL CONSTRAINT [DF_ProductInfos_FitTemperatorRange]  DEFAULT ('Normal'),
	[FitHighTemperature] [decimal](18, 2) NULL,
	[IsDeleted] [int] NOT NULL CONSTRAINT [DF_ProductInfos_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_ProductInfos] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductInStoreRecordInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductInStoreRecordInfos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[SRegionId] [int] NOT NULL,
	[ProductCount] [int] NOT NULL,
	[InStoreTime] [datetime] NOT NULL CONSTRAINT [DF_ProInStoreRecordInfos_InStoreTime]  DEFAULT (getdate()),
	[IsDeleted] [int] NOT NULL CONSTRAINT [DF_ProInStoreRecordInfos_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_ProInStoreRecordInfos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductStoreInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductStoreInfos](
	[ProStoreId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[StoreId] [int] NOT NULL,
	[SRegionId] [int] NOT NULL,
	[ProductCount] [int] NOT NULL CONSTRAINT [DF_ProductStoreInfos_ProductCount]  DEFAULT ((0)),
	[IsDeleted] [int] NULL CONSTRAINT [DF_ProductStoreInfos_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_ProductStoreInfos] PRIMARY KEY CLUSTERED 
(
	[ProStoreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StoreInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StoreInfos](
	[StoreId] [int] IDENTITY(1,1) NOT NULL,
	[StoreName] [nvarchar](20) NOT NULL,
	[StoreNo] [varchar](10) NULL,
	[RegionCount] [int] NOT NULL CONSTRAINT [DF_StoreInfos_RegionCount]  DEFAULT ((0)),
	[Remark] [nvarchar](500) NULL,
	[IsDeleted] [int] NULL CONSTRAINT [DF_StoreInfos_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_StoreInfos] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StoreRegionInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StoreRegionInfos](
	[SRegionId] [int] IDENTITY(1,1) NOT NULL,
	[SRegionName] [varchar](20) NOT NULL,
	[SRegionNo] [varchar](10) NULL,
	[StoreId] [int] NOT NULL,
	[SRTemperature] [decimal](18, 2) NULL,
	[AllowLowTemperature] [decimal](18, 2) NULL,
	[AllowHighTemperature] [decimal](18, 2) NULL,
	[TemperState] [int] NOT NULL CONSTRAINT [DF_StoreRegionInfos_TemperState]  DEFAULT ((1)),
	[Remark] [nvarchar](500) NULL,
	[IsDeleted] [int] NULL CONSTRAINT [DF_StoreRegionInfos_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_StoreRegionInfos] PRIMARY KEY CLUSTERED 
(
	[SRegionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserInfos](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](20) NOT NULL,
	[UserPwd] [varchar](50) NOT NULL,
	[UserState] [int] NOT NULL CONSTRAINT [DF_UserInfos_UserState]  DEFAULT ((1)),
	[IsDeleted] [int] NOT NULL CONSTRAINT [DF_UserInfos_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_UserInfos] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ViewProductStoreInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewProductStoreInfos]
AS
SELECT  dbo.ProductStoreInfos.ProStoreId, dbo.ProductStoreInfos.ProductId, dbo.ProductInfos.ProductNo, 
                   dbo.ProductInfos.ProductName, dbo.ProductStoreInfos.StoreId, dbo.StoreInfos.StoreName, 
                   dbo.ProductStoreInfos.SRegionId, dbo.StoreRegionInfos.SRegionName, dbo.ProductStoreInfos.ProductCount, 
                   dbo.ProductStoreInfos.IsDeleted
FROM      dbo.ProductInfos INNER JOIN
                   dbo.ProductStoreInfos ON dbo.ProductInfos.ProductId = dbo.ProductStoreInfos.ProductId INNER JOIN
                   dbo.StoreInfos ON dbo.ProductStoreInfos.StoreId = dbo.StoreInfos.StoreId INNER JOIN
                   dbo.StoreRegionInfos ON dbo.ProductStoreInfos.SRegionId = dbo.StoreRegionInfos.SRegionId

GO
/****** Object:  View [dbo].[ViewSRegionTemperInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[ViewSRegionTemperInfos]
as
select t.* ,r.SRTemperature,r.AllowLowTemperature,r.AllowHighTemperature,r.TemperState from (
select s.StoreId,StoreName,sr.SRegionId,sum(ProductCount) TotalCount,SRegionName
from StoreRegionInfos sr
inner join StoreInfos s on s.StoreId=sr.StoreId
left join ProductStoreInfos ps on ps.SRegionId=sr.SRegionId
where sr.IsDeleted=0 and s.IsDeleted=0 and ps.IsDeleted=0
group by s.StoreId,StoreName,sr.SRegionId,SRegionName

) as  t
inner join StoreRegionInfos r on r.SRegionId=t.SRegionId
GO
/****** Object:  View [dbo].[ViewStoreRegionInfos]    Script Date: 2021/4/12 09:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewStoreRegionInfos]
AS
SELECT  dbo.StoreRegionInfos.SRegionId, dbo.StoreRegionInfos.SRegionName, dbo.StoreRegionInfos.SRegionNo, 
                   dbo.StoreRegionInfos.StoreId, dbo.StoreInfos.StoreName, dbo.StoreRegionInfos.SRTemperature, 
                   dbo.StoreRegionInfos.AllowLowTemperature, dbo.StoreRegionInfos.AllowHighTemperature, 
                   dbo.StoreRegionInfos.TemperState, 
                   (CASE TemperState WHEN 1 THEN '正常' WHEN 0 THEN '低温' WHEN 2 THEN '高温' ELSE '' END) AS TemperStateText, 
                   dbo.StoreRegionInfos.Remark, dbo.StoreRegionInfos.IsDeleted
FROM      dbo.StoreRegionInfos INNER JOIN
                   dbo.StoreInfos ON dbo.StoreRegionInfos.StoreId = dbo.StoreInfos.StoreId

GO
SET IDENTITY_INSERT [dbo].[ProductInfos] ON 

INSERT [dbo].[ProductInfos] ([ProductId], [ProductNo], [ProductName], [FitLowTemperature], [FitHighTemperature], [IsDeleted]) VALUES (1, N'10001', N'伊利牛奶', CAST(3.00 AS Decimal(18, 2)), CAST(27.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[ProductInfos] ([ProductId], [ProductNo], [ProductName], [FitLowTemperature], [FitHighTemperature], [IsDeleted]) VALUES (2, N'10002', N'可口可乐', CAST(5.00 AS Decimal(18, 2)), CAST(26.50 AS Decimal(18, 2)), 0)
INSERT [dbo].[ProductInfos] ([ProductId], [ProductNo], [ProductName], [FitLowTemperature], [FitHighTemperature], [IsDeleted]) VALUES (3, N'10003', N'蒙牛牛奶', CAST(24.50 AS Decimal(18, 2)), CAST(27.50 AS Decimal(18, 2)), 0)
INSERT [dbo].[ProductInfos] ([ProductId], [ProductNo], [ProductName], [FitLowTemperature], [FitHighTemperature], [IsDeleted]) VALUES (4, N'20001', N'特步休闲鞋-女', CAST(18.00 AS Decimal(18, 2)), CAST(32.00 AS Decimal(18, 2)), 0)
SET IDENTITY_INSERT [dbo].[ProductInfos] OFF
SET IDENTITY_INSERT [dbo].[ProductInStoreRecordInfos] ON 

INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (1, 1, 1, 100, CAST(N'2021-03-20 23:03:41.610' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (2, 2, 1, 200, CAST(N'2021-03-20 23:03:51.950' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (3, 1, 5, 50, CAST(N'2021-03-20 23:04:17.740' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (4, 2, 5, 200, CAST(N'2021-03-20 23:04:43.420' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (5, 1, 1, 200, CAST(N'2021-03-20 23:36:42.063' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (6, 3, 4, 100, CAST(N'2021-03-21 00:04:29.037' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (7, 1, 6, 50, CAST(N'2021-03-27 23:02:54.537' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (8, 1, 6, 100, CAST(N'2021-03-27 23:03:23.110' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (9, 2, 1, 100, CAST(N'2021-03-27 23:13:21.010' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (10, 2, 1, 100, CAST(N'2021-03-27 23:13:43.717' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (11, 1, 5, 100, CAST(N'2021-03-27 23:16:03.860' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (12, 1, 5, 100, CAST(N'2021-03-27 23:16:21.603' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (13, 1, 7, 100, CAST(N'2021-03-28 21:41:08.847' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (14, 1, 7, 50, CAST(N'2021-03-28 21:41:20.900' AS DateTime), 0)
INSERT [dbo].[ProductInStoreRecordInfos] ([Id], [ProductId], [SRegionId], [ProductCount], [InStoreTime], [IsDeleted]) VALUES (15, 3, 8, 50, CAST(N'2021-03-28 21:48:09.320' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[ProductInStoreRecordInfos] OFF
SET IDENTITY_INSERT [dbo].[ProductStoreInfos] ON 

INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (1, 1, 1, 1, 300, 0)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (2, 2, 1, 1, 400, 1)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (3, 1, 2, 5, 250, 0)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (4, 2, 2, 5, 200, 0)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (5, 3, 1, 4, 100, 0)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (6, 1, 2, 6, 150, 0)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (7, 1, 3, 7, 150, 0)
INSERT [dbo].[ProductStoreInfos] ([ProStoreId], [ProductId], [StoreId], [SRegionId], [ProductCount], [IsDeleted]) VALUES (8, 3, 3, 8, 50, 0)
SET IDENTITY_INSERT [dbo].[ProductStoreInfos] OFF
SET IDENTITY_INSERT [dbo].[StoreInfos] ON 

INSERT [dbo].[StoreInfos] ([StoreId], [StoreName], [StoreNo], [RegionCount], [Remark], [IsDeleted]) VALUES (1, N'第一仓库', N'01', 2, N'这是01号仓库。', 0)
INSERT [dbo].[StoreInfos] ([StoreId], [StoreName], [StoreNo], [RegionCount], [Remark], [IsDeleted]) VALUES (2, N'第二仓库', N'02', 2, N'这是第二仓库。', 0)
INSERT [dbo].[StoreInfos] ([StoreId], [StoreName], [StoreNo], [RegionCount], [Remark], [IsDeleted]) VALUES (3, N'第三仓库', N'03', 3, N'这是第03仓库', 0)
INSERT [dbo].[StoreInfos] ([StoreId], [StoreName], [StoreNo], [RegionCount], [Remark], [IsDeleted]) VALUES (4, N'第四仓库', N'04', 0, N'', 1)
SET IDENTITY_INSERT [dbo].[StoreInfos] OFF
SET IDENTITY_INSERT [dbo].[StoreRegionInfos] ON 

INSERT [dbo].[StoreRegionInfos] ([SRegionId], [SRegionName], [SRegionNo], [StoreId], [SRTemperature], [AllowLowTemperature], [AllowHighTemperature], [TemperState], [Remark], [IsDeleted]) VALUES (1, N'01-01', N'0101', 1, CAST(27.00 AS Decimal(18, 2)), CAST(25.50 AS Decimal(18, 2)), CAST(27.50 AS Decimal(18, 2)), 1, N'第1仓库第2区', 0)
INSERT [dbo].[StoreRegionInfos] ([SRegionId], [SRegionName], [SRegionNo], [StoreId], [SRTemperature], [AllowLowTemperature], [AllowHighTemperature], [TemperState], [Remark], [IsDeleted]) VALUES (4, N'01-02区', N'0102', 1, CAST(25.00 AS Decimal(18, 2)), CAST(24.50 AS Decimal(18, 2)), CAST(27.50 AS Decimal(18, 2)), 1, N'第一仓库第2区。', 0)
INSERT [dbo].[StoreRegionInfos] ([SRegionId], [SRegionName], [SRegionNo], [StoreId], [SRTemperature], [AllowLowTemperature], [AllowHighTemperature], [TemperState], [Remark], [IsDeleted]) VALUES (5, N'02-01', N'0201', 2, CAST(26.00 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), CAST(27.00 AS Decimal(18, 2)), 1, N'第二仓库第1区。', 0)
INSERT [dbo].[StoreRegionInfos] ([SRegionId], [SRegionName], [SRegionNo], [StoreId], [SRTemperature], [AllowLowTemperature], [AllowHighTemperature], [TemperState], [Remark], [IsDeleted]) VALUES (6, N'02-02区', N'0202', 2, CAST(25.50 AS Decimal(18, 2)), CAST(25.00 AS Decimal(18, 2)), CAST(28.00 AS Decimal(18, 2)), 1, N'第2仓库第2区', 0)
INSERT [dbo].[StoreRegionInfos] ([SRegionId], [SRegionName], [SRegionNo], [StoreId], [SRTemperature], [AllowLowTemperature], [AllowHighTemperature], [TemperState], [Remark], [IsDeleted]) VALUES (7, N'03-01', N'0301', 3, CAST(25.00 AS Decimal(18, 2)), CAST(22.00 AS Decimal(18, 2)), CAST(29.00 AS Decimal(18, 2)), 1, N'', 0)
INSERT [dbo].[StoreRegionInfos] ([SRegionId], [SRegionName], [SRegionNo], [StoreId], [SRTemperature], [AllowLowTemperature], [AllowHighTemperature], [TemperState], [Remark], [IsDeleted]) VALUES (8, N'03-02', N'0302', 3, CAST(27.50 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(32.00 AS Decimal(18, 2)), 1, N'', 0)
SET IDENTITY_INSERT [dbo].[StoreRegionInfos] OFF
SET IDENTITY_INSERT [dbo].[UserInfos] ON 

INSERT [dbo].[UserInfos] ([UserId], [UserName], [UserPwd], [UserState], [IsDeleted]) VALUES (1, N'admin', N'admin', 1, 0)
INSERT [dbo].[UserInfos] ([UserId], [UserName], [UserPwd], [UserState], [IsDeleted]) VALUES (2, N'lyc001', N'123456', 1, 0)
SET IDENTITY_INSERT [dbo].[UserInfos] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[6] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProductInfos"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProductStoreInfos"
            Begin Extent = 
               Top = 9
               Left = 272
               Bottom = 172
               Right = 469
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "StoreInfos"
            Begin Extent = 
               Top = 7
               Left = 546
               Bottom = 170
               Right = 737
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "StoreRegionInfos"
            Begin Extent = 
               Top = 243
               Left = 314
               Bottom = 406
               Right = 580
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewProductStoreInfos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewProductStoreInfos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "StoreRegionInfos"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 211
               Right = 324
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "StoreInfos"
            Begin Extent = 
               Top = 7
               Left = 362
               Bottom = 231
               Right = 588
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewStoreRegionInfos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewStoreRegionInfos'
GO
USE [master]
GO
ALTER DATABASE [STMSNewDBase] SET  READ_WRITE 
GO
