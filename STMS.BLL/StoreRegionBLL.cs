﻿using STMS.DAL;
using STMS.Models.VModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STMS.BLL
{
    public class StoreRegionBLL
    {
        ViewStoreRegionDAL vsrDAL = new ViewStoreRegionDAL();

        /// <summary>
        /// 条件查询出库分区列表，条件拼接
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="stateId"></param>
        /// <param name="keywords"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public List<ViewStoreRegionInfo> FindStoreRegionList(int storeId, int stateId, string keywords, bool showDel)
        {
            int isDeleted = showDel ? 0 : 1;
            return vsrDAL.FindStoreRegionList(storeId, stateId, keywords, isDeleted);
        }
    }
}
