﻿using STMS.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STMS.STMSApp
{
   


    public partial class 登录页面窗体 : Form
    {
        public 登录页面窗体()
        {
            InitializeComponent();
            //txtUName.Clear();
            //txtUPwd.Clear();
            txtUName.Text = "admin";
            txtUPwd.Text = "admin";
        }
        /// <summary>
        /// 登录过程处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            //接收页面输入
            string uName= txtUName.Text.Trim();
            string uPwd= txtUPwd.Text.Trim();
            //检查信息非空检查
            if (string.IsNullOrEmpty(uName))
            {
                MsgBoxHelper.MsgErrorShow("登录系统", "账户不能为空！");
                txtUName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(uPwd))
            {
                MsgBoxHelper.MsgErrorShow("登录系统", "密码不能为空！");
                txtUPwd.Focus();
                return;
            }
            //登录过程检查用户的存在性  ui--bll--dal,检查（select）--核心
            UserBLL userBLL = new UserBLL();
            bool bLogin = userBLL.LoginSystem(uName, uPwd);
            //检查结果，作处理：存在成功，显主面；不存在，中断
            if (bLogin)//登录成功
            {
                //显示系统主页
                FrmMain fMain = new FrmMain();
                fMain.Tag = uName;
                fMain.Show();
                this.Hide();
            }
            else
            {
                MsgBoxHelper.MsgErrorShow("登录系统", "账户或密码错误！");
                txtUName.Focus();
            }
        }

        /// <summary>
        /// 点机关闭按钮，询问是否退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MsgBoxHelper.MsgBoxConform("退出系统", "确定要退出？") == DialogResult.Yes)
            {

                Application.ExitThread();
                
            }
            else
            {
                e.Cancel = true;
            }
            
        }
    }
}
