﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STMS.STMSApp
{
    
    public class MsgBoxHelper
    {
        /// <summary>
        /// 信息提示框 成功
        /// </summary>
        /// <param name="title"></param>
        /// <param name="Msg"></param>
        /// <returns></returns>
        public static DialogResult MsgBoxShow(string title, string Msg)
        {
            return MessageBox.Show(Msg, title,
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 问题询问框
        /// </summary>
        /// <param name="title"></param>
        /// <param name="Msg"></param>
        /// <returns></returns>
        public static DialogResult MsgBoxConform(string title, string Msg)
        {
            return MessageBox.Show(Msg, title,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static DialogResult MsgBoxConfirm(string title, string Msg)
        {
            return MessageBox.Show(Msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// 错误消息框
        /// </summary>
        /// <param name="title"></param>
        /// <param name="Msg"></param>
        public static void MsgErrorShow(string title, string Msg)
        {
            MessageBox.Show(Msg, title,
               MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
