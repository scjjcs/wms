﻿namespace STMS.STMSApp
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelPage = new System.Windows.Forms.Panel();
            this.btnClosePage = new System.Windows.Forms.Button();
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblLoginTime = new System.Windows.Forms.Label();
            this.lblLoginUser = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelPageTop = new System.Windows.Forms.Panel();
            this.出库页面按钮 = new STMS.STMSApp.UControls.UPageButton();
            this.btnStoreTemperPage = new STMS.STMSApp.UControls.UPageButton();
            this.btnStorePage = new STMS.STMSApp.UControls.UPageButton();
            this.btnSRegionPage = new STMS.STMSApp.UControls.UPageButton();
            this.btnProductInStorePage = new STMS.STMSApp.UControls.UPageButton();
            this.btnProductPage = new STMS.STMSApp.UControls.UPageButton();
            this.uPanel1 = new STMS.STMSApp.UControls.UPanel();
            this.温度模块按钮 = new STMS.STMSApp.UControls.UMenuButton();
            this.产品模块按钮 = new STMS.STMSApp.UControls.UMenuButton();
            this.仓库模块按钮 = new STMS.STMSApp.UControls.UMenuButton();
            this.panel1.SuspendLayout();
            this.panelPageTop.SuspendLayout();
            this.uPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPage
            // 
            this.panelPage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPage.AutoScroll = true;
            this.panelPage.AutoSize = true;
            this.panelPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(134)))));
            this.panelPage.Location = new System.Drawing.Point(234, 126);
            this.panelPage.Margin = new System.Windows.Forms.Padding(2);
            this.panelPage.Name = "panelPage";
            this.panelPage.Size = new System.Drawing.Size(980, 417);
            this.panelPage.TabIndex = 10;
            this.panelPage.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPage_Paint);
            // 
            // btnClosePage
            // 
            this.btnClosePage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClosePage.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnClosePage.FlatAppearance.BorderSize = 0;
            this.btnClosePage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClosePage.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClosePage.ForeColor = System.Drawing.Color.White;
            this.btnClosePage.Location = new System.Drawing.Point(803, 3);
            this.btnClosePage.Name = "btnClosePage";
            this.btnClosePage.Size = new System.Drawing.Size(44, 40);
            this.btnClosePage.TabIndex = 19;
            this.btnClosePage.Text = "X";
            this.btnClosePage.UseVisualStyleBackColor = false;
            this.btnClosePage.Click += new System.EventHandler(this.btnClosePage_Click);
            // 
            // panelTop
            // 
            this.panelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTop.BackgroundImage = global::STMS.STMSApp.Properties.Resources.logo;
            this.panelTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Margin = new System.Windows.Forms.Padding(2);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1224, 60);
            this.panelTop.TabIndex = 12;
            // 
            // lblLoginTime
            // 
            this.lblLoginTime.AutoSize = true;
            this.lblLoginTime.Font = new System.Drawing.Font("微软雅黑", 10.8F);
            this.lblLoginTime.ForeColor = System.Drawing.Color.Silver;
            this.lblLoginTime.Location = new System.Drawing.Point(224, 12);
            this.lblLoginTime.Name = "lblLoginTime";
            this.lblLoginTime.Size = new System.Drawing.Size(84, 20);
            this.lblLoginTime.TabIndex = 3;
            this.lblLoginTime.Text = "登录时间：";
            // 
            // lblLoginUser
            // 
            this.lblLoginUser.AutoSize = true;
            this.lblLoginUser.Font = new System.Drawing.Font("微软雅黑", 10.8F);
            this.lblLoginUser.ForeColor = System.Drawing.Color.Silver;
            this.lblLoginUser.Location = new System.Drawing.Point(6, 12);
            this.lblLoginUser.Name = "lblLoginUser";
            this.lblLoginUser.Size = new System.Drawing.Size(69, 20);
            this.lblLoginUser.TabIndex = 20;
            this.lblLoginUser.Text = "登录者：";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(44)))), ((int)(((byte)(102)))));
            this.panel1.Controls.Add(this.lblLoginUser);
            this.panel1.Controls.Add(this.lblLoginTime);
            this.panel1.Location = new System.Drawing.Point(6, 548);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1218, 41);
            this.panel1.TabIndex = 21;
            // 
            // panelPageTop
            // 
            this.panelPageTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPageTop.Controls.Add(this.btnClosePage);
            this.panelPageTop.Controls.Add(this.出库页面按钮);
            this.panelPageTop.Controls.Add(this.btnStoreTemperPage);
            this.panelPageTop.Controls.Add(this.btnStorePage);
            this.panelPageTop.Controls.Add(this.btnSRegionPage);
            this.panelPageTop.Controls.Add(this.btnProductInStorePage);
            this.panelPageTop.Controls.Add(this.btnProductPage);
            this.panelPageTop.Location = new System.Drawing.Point(234, 69);
            this.panelPageTop.Name = "panelPageTop";
            this.panelPageTop.Size = new System.Drawing.Size(980, 50);
            this.panelPageTop.TabIndex = 22;
            // 
            // 出库页面按钮
            // 
            this.出库页面按钮.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.出库页面按钮.BackColor = System.Drawing.Color.Transparent;
            this.出库页面按钮.BackgroundImage = global::STMS.STMSApp.Properties.Resources.btnbg01;
            this.出库页面按钮.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.出库页面按钮.BtnText = "产品出库页面";
            this.出库页面按钮.Location = new System.Drawing.Point(563, 3);
            this.出库页面按钮.Name = "出库页面按钮";
            this.出库页面按钮.Size = new System.Drawing.Size(106, 40);
            this.出库页面按钮.TabIndex = 18;
            // 
            // btnStoreTemperPage
            // 
            this.btnStoreTemperPage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStoreTemperPage.BackColor = System.Drawing.Color.Transparent;
            this.btnStoreTemperPage.BackgroundImage = global::STMS.STMSApp.Properties.Resources.btnbg01;
            this.btnStoreTemperPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStoreTemperPage.BtnText = "仓库温控页面";
            this.btnStoreTemperPage.Location = new System.Drawing.Point(675, 3);
            this.btnStoreTemperPage.Name = "btnStoreTemperPage";
            this.btnStoreTemperPage.Size = new System.Drawing.Size(109, 40);
            this.btnStoreTemperPage.TabIndex = 18;
            // 
            // btnStorePage
            // 
            this.btnStorePage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStorePage.BackColor = System.Drawing.Color.Transparent;
            this.btnStorePage.BackgroundImage = global::STMS.STMSApp.Properties.Resources.btnbg01;
            this.btnStorePage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStorePage.BtnText = "仓库管理页面";
            this.btnStorePage.Location = new System.Drawing.Point(106, 3);
            this.btnStorePage.Name = "btnStorePage";
            this.btnStorePage.Size = new System.Drawing.Size(109, 40);
            this.btnStorePage.TabIndex = 14;
            this.btnStorePage.Click += new System.EventHandler(this.仓库管理页面按钮_Click);
            // 
            // btnSRegionPage
            // 
            this.btnSRegionPage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSRegionPage.BackColor = System.Drawing.Color.Transparent;
            this.btnSRegionPage.BackgroundImage = global::STMS.STMSApp.Properties.Resources.btnbg01;
            this.btnSRegionPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSRegionPage.BtnText = "分区管理页面";
            this.btnSRegionPage.Location = new System.Drawing.Point(221, 3);
            this.btnSRegionPage.Name = "btnSRegionPage";
            this.btnSRegionPage.Size = new System.Drawing.Size(106, 40);
            this.btnSRegionPage.TabIndex = 15;
            // 
            // btnProductInStorePage
            // 
            this.btnProductInStorePage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnProductInStorePage.BackColor = System.Drawing.Color.Transparent;
            this.btnProductInStorePage.BackgroundImage = global::STMS.STMSApp.Properties.Resources.btnbg01;
            this.btnProductInStorePage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnProductInStorePage.BtnText = "产品入库页面";
            this.btnProductInStorePage.Location = new System.Drawing.Point(448, 3);
            this.btnProductInStorePage.Name = "btnProductInStorePage";
            this.btnProductInStorePage.Size = new System.Drawing.Size(109, 40);
            this.btnProductInStorePage.TabIndex = 17;
            // 
            // btnProductPage
            // 
            this.btnProductPage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnProductPage.BackColor = System.Drawing.Color.Transparent;
            this.btnProductPage.BackgroundImage = global::STMS.STMSApp.Properties.Resources.btnbg01;
            this.btnProductPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnProductPage.BtnText = "产品管理页面";
            this.btnProductPage.Location = new System.Drawing.Point(333, 3);
            this.btnProductPage.Name = "btnProductPage";
            this.btnProductPage.Size = new System.Drawing.Size(109, 40);
            this.btnProductPage.TabIndex = 16;
            // 
            // uPanel1
            // 
            this.uPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.uPanel1.BackColor = System.Drawing.Color.Transparent;
            this.uPanel1.BgColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(50)))), ((int)(((byte)(116)))));
            this.uPanel1.BgColor2 = System.Drawing.Color.Transparent;
            this.uPanel1.BorderColor = System.Drawing.Color.Red;
            this.uPanel1.Controls.Add(this.温度模块按钮);
            this.uPanel1.Controls.Add(this.产品模块按钮);
            this.uPanel1.Controls.Add(this.仓库模块按钮);
            this.uPanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.uPanel1.Location = new System.Drawing.Point(6, 80);
            this.uPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.uPanel1.Name = "uPanel1";
            this.uPanel1.Radius = 8;
            this.uPanel1.Size = new System.Drawing.Size(206, 463);
            this.uPanel1.TabIndex = 13;
            // 
            // 温度模块按钮
            // 
            this.温度模块按钮.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(50)))), ((int)(((byte)(116)))));
            this.温度模块按钮.BtnImg = global::STMS.STMSApp.Properties.Resources._04;
            this.温度模块按钮.BtnText = "温度模块";
            this.温度模块按钮.Location = new System.Drawing.Point(16, 172);
            this.温度模块按钮.Name = "温度模块按钮";
            this.温度模块按钮.Size = new System.Drawing.Size(156, 55);
            this.温度模块按钮.TabIndex = 2;
            this.温度模块按钮.Click += new System.EventHandler(this.温度模块按钮_Click);
            // 
            // 产品模块按钮
            // 
            this.产品模块按钮.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(50)))), ((int)(((byte)(116)))));
            this.产品模块按钮.BtnImg = global::STMS.STMSApp.Properties.Resources._03;
            this.产品模块按钮.BtnText = "产品模块";
            this.产品模块按钮.Location = new System.Drawing.Point(16, 92);
            this.产品模块按钮.Name = "产品模块按钮";
            this.产品模块按钮.Size = new System.Drawing.Size(156, 55);
            this.产品模块按钮.TabIndex = 1;
            this.产品模块按钮.Click += new System.EventHandler(this.产品模块按钮_Click);
            // 
            // 仓库模块按钮
            // 
            this.仓库模块按钮.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(50)))), ((int)(((byte)(116)))));
            this.仓库模块按钮.BtnImg = global::STMS.STMSApp.Properties.Resources._02;
            this.仓库模块按钮.BtnText = "仓库模块";
            this.仓库模块按钮.Location = new System.Drawing.Point(16, 18);
            this.仓库模块按钮.Name = "仓库模块按钮";
            this.仓库模块按钮.Size = new System.Drawing.Size(156, 55);
            this.仓库模块按钮.TabIndex = 0;
            this.仓库模块按钮.Click += new System.EventHandler(this.仓库模块按钮_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(61)))), ((int)(((byte)(134)))));
            this.ClientSize = new System.Drawing.Size(1236, 601);
            this.Controls.Add(this.panelPageTop);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uPanel1);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelPage);
            this.Name = "FrmMain";
            this.Text = "仓库温控系统主页面";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.SizeChanged += new System.EventHandler(this.主页面窗体_SizeChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelPageTop.ResumeLayout(false);
            this.uPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UControls.UMenuButton 仓库模块按钮;
        private System.Windows.Forms.Panel panelPage;
        private System.Windows.Forms.Panel panelTop;
        private UControls.UPanel uPanel1;
        private UControls.UPageButton btnStorePage;
        private UControls.UPageButton btnSRegionPage;
        private UControls.UPageButton btnProductPage;
        private UControls.UPageButton btnProductInStorePage;
        private UControls.UPageButton btnStoreTemperPage;
        private UControls.UMenuButton 温度模块按钮;
        private UControls.UMenuButton 产品模块按钮;
        private System.Windows.Forms.Button btnClosePage;
        private System.Windows.Forms.Label lblLoginTime;
        private System.Windows.Forms.Label lblLoginUser;
        private System.Windows.Forms.Panel panel1;
        private UControls.UPageButton 出库页面按钮;
        private System.Windows.Forms.Panel panelPageTop;
    }
}