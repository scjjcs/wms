﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STMS.STMSApp.FModels
{
    public class FInfoData
    {
        /// <summary>
        /// 1 add  2 edit  3 addchild添加子类  4 info详情
        /// </summary>
        public int ActType {  get; set; }
        
        /// <summary>
        /// 主键编号
        /// </summary>
        public int FId { get; set; }
    }
}
