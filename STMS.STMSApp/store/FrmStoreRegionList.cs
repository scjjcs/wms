﻿using STMS.STMSApp.FModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STMS.STMSApp.store
{
    public partial class FrmStoreRegionList : Form
    {
        public FrmStoreRegionList()
        {
            InitializeComponent();
        }
        public event Action ReLoadStoreList;//刷新仓库管理列表页数据
        private void FrmStoreRegionList_Load(object sender, EventArgs e)
        {
            txtKeyWords.Clear();
            chkShowDel.Checked = false;
            //加载仓库下拉框
            FormUtility.LoadCboStores(cboStores);
            //加载状态下拉框
            LoadCboStates();
        }

        private void LoadCboStates()
        {
            List<RegionStateModel> stateList=FormUtility.RegionStateList();
            cboStatus.DisplayMember = "StateText";
            cboStatus.ValueMember = "RegionState";
            cboStatus.DataSource = stateList;
            cboStatus.SelectedIndex= 0;

        }
    }
}
