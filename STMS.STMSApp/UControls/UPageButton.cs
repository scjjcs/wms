﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STMS.STMSApp.UControls
{
    [DefaultEvent("Click")]//将单击事件设为默认事件
    public partial class UPageButton : UserControl
    {
        public UPageButton()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 按钮文本的设置
        /// </summary>
        public string BtnText
        {
            get
            {
                return lblText.Text;
            }
            set { lblText.Text = value; }
        }

        public override Color ForeColor { get => lblText.ForeColor; set => lblText.ForeColor = value; }

        protected override void OnClick(EventArgs e)
        {
            lblText.ForeColor = Color.FromArgb(45, 50, 116);
            base.OnClick(e);
        }

        private void lblText_Click(object sender, EventArgs e)
        {
            //将标签上的点击关联到整体控件的点击事件上
            base.OnClick(e);
            
        }
    }
}
