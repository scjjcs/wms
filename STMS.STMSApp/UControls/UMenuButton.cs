﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 UMenuButton制作：

1. 外观布局

2. 添加扩展属性：图标、按钮文本

3. 重OnMouseClick/OnMouseEnter/OnMouseLeave

4. 整个控件的Click事件处理
 
 */
namespace STMS.STMSApp.UControls
{
    [DefaultEvent("Click")]
    public partial class UMenuButton : UserControl
    {
        public UMenuButton()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 按钮图标
        /// </summary>
        public Image BtnImg
        {
            get { return pbImg.Image; }
            set
            {
                pbImg.Image = value;
            }
        }

        /// <summary>
        /// 按钮文本
        /// </summary>
        public string BtnText
        {
            get { return lblBtnText.Text; }
            set
            {
                lblBtnText.Text= value;
            }
        }

        //重写鼠标移入，绑定鼠标移入事件并改变鼠标移入后控件的颜色
        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this.BackColor = Color.FromArgb(88, 116, 216);
        }

        //重写鼠标移出，绑定鼠标移出事件并更改鼠标移开后控件的颜色
        protected override void OnMouseLeave(EventArgs e)
        {
             base.OnMouseLeave(e);
            this.BackColor = Color.FromArgb(45, 50, 116);
        }

        //点击时的颜色
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            this.BackColor = Color.FromArgb(88, 116, 216);
        }

        private void lblBtnText_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void pbImg_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void lblBtnText_MouseEnter(object sender, EventArgs e)
        {
            OnMouseEnter(e);
        }

        private void pbImg_MouseEnter(object sender, EventArgs e)
        {
            OnMouseEnter(e);
        }
    }
}
