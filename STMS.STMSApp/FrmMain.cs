﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using STMS.STMSApp.store;
using STMS.STMSApp.UControls;

namespace STMS.STMSApp
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        System.Timers.Timer timer = null;
        int selModuleCode = 1;// 1 store        //2 product        //3 temper
        private void FrmMain_Load(object sender, EventArgs e)
        {
            if (this.Tag != null)//this.tag用来传递用户名
            {
                InitStatusInfo();//状态栏信息初始化
                ShowStoreManage();//显示仓库管理页面
                btnClosePage.Visible = true;
                RegisterPageBtnClick();//    注册分页按钮的单击事件_方法
                InitPageBtnTag();
            }
        }
        /// <summary>
        /// 设置分页按钮的Tag属性--页面关联
        /// </summary>
        private void InitPageBtnTag()
        {
            btnStorePage.Tag = typeof(store.FrmStoreList).FullName;//命名空间+FrmStoreList
            btnSRegionPage.Tag = typeof(store.FrmStoreRegionList).FullName;
            btnProductPage.Tag = typeof(product.FrmProductList).FullName;
            btnProductInStorePage.Tag = typeof(product.FrmProductInStore).FullName;
            出库页面按钮.Tag = typeof(product.出库页面).FullName;
            btnStoreTemperPage.Tag = typeof(storeTemper.FrmStoreRegionTemperatureList).FullName;
        }



        /// <summary>
        /// 显示仓库管理页面
        /// </summary>
        private void ShowStoreManage()
        {
            //selModuleCode = 1;
            SetModuleBtnBackClolor(1);
            FrmStoreList fStoreList = new store.FrmStoreList();
            panelPage.AddPanelForm(fStoreList);
        }

        /// <summary>
        /// 状态栏信息初始化
        /// </summary>
        private void InitStatusInfo()
        {
            string uName = this.Tag.ToString();
            lblLoginUser.Text = uName;

            //时间 动态显示
            lblLoginTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.AutoReset = true;     //重复执行
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.Invoke(new Action(() =>           //切换线程，执行委托
        {
            this.lblLoginTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        }));
        }












        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MsgBoxHelper.MsgBoxConform("退出系统", "确定退出？") == DialogResult.Yes)
            {
                if (timer != null)
                {
                    timer.Stop();
                }
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 注册分页按钮的单击事件
        /// </summary>
        private void RegisterPageBtnClick()
        {
            foreach (Control c in panelPageTop.Controls)
            {
                if (c is UPageButton)
                {
                    UPageButton btn = c as UPageButton;
                    btn.Click += btnPage_Click;
                }
            }
        }



        /// <summary>
        /// 分页按钮单击处理程序
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        Form fff;

        private void btnPage_Click(object sender, EventArgs e)
        {
            //分页按钮文本颜色
            Color 选中时显示的颜色 = Color.White;//点击时或显示相关页面时
            Color 未选中的显示的颜色 = Color.FromArgb(45, 50, 116);//默认或没有显示页面时

            UPageButton clickBtn = sender as UPageButton;//当前点击按钮


            foreach (Control c in panelPageTop.Controls)
            {
                if (c is UPageButton)
                {
                    UPageButton btn = c as UPageButton;
                    if (btn.Name != clickBtn.Name)
                    {
                        btn.ForeColor = 未选中的显示的颜色;
                        btn.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;
                    }
                    else
                    {
                        btn.ForeColor = 选中时显示的颜色;
                        btn.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg02;
                    }
                }

            }
            //切换页面的实现
            if (clickBtn.Tag != null)
            {
                string frmFullName = clickBtn.Tag.ToString();
                int lastIndex = frmFullName.LastIndexOf('.');
                string frmName = frmFullName.Substring(lastIndex + 1);
                Form f = FormUtility.GetOpenForm(frmName);

                if (f == null)
                {
                    //实例化，动态实例化对象的过程
                    f = (Form)Activator.CreateInstance(Type.GetType(frmFullName));//（Form）的作用：将Object类型转换为Form类型。
                    fff = f;

                }
                panelPage.AddPanelForm(f);
                ShowClosePageBtn();
            }
            //切换页面的实现();
            //是否处理关闭按钮的显示

        }




        //有页面打开才显示关闭按钮
        /// <summary>
        /// 页面关闭按钮的显示
        /// </summary>
        private void ShowClosePageBtn()
        {
            if (panelPage.Controls.Count > 0)
            {
                btnClosePage.Visible = true;
            }
            else
            {
                btnClosePage.Visible = false;
            }
        }
        ////////10月12日晚，检测到这里
        /// <summary>
        /// 设置模块按钮背景色
        /// </summary>
        /// <param name="code"></param>
        private void SetModuleBtnBackClolor(int code)
        {
            switch (code)
            {
                case 1:
                    //设置仓库管理模块按钮的背景色，其他两个按钮背景--默认背景
                    SetMenuBtnsColor(仓库模块按钮, btnStorePage);
                    break;
                case 2:
                    //设置产品管理模块按钮的背景色，其他两个按钮背景--默认背景
                    SetMenuBtnsColor(产品模块按钮, btnProductPage);
                    break;
                case 3:
                    //设置仓库温控管理模块按钮的背景色，其他两个按钮背景--默认背景
                    SetMenuBtnsColor(温度模块按钮, btnStoreTemperPage);
                    break;
            }
        }

        /// <summary>
        /// 处理左边按钮背景色和分页按钮的文本
        /// </summary>
        private void SetMenuBtnsColor(UMenuButton btn, UPageButton btnPage)
        {
            //模块按钮背景色，未点击
            Color unSelColor = Color.FromArgb(45, 50, 116);//未选中
            Color SelColor = Color.FromArgb(88, 116, 216);//选中

            //分页按钮颜色，
            Color topUnSelColor = Color.FromArgb(45, 50, 116);//点击或显示相关页面时
            Color topSelColor = Color.White;//默认或取消时

            Image topUnSelimage = Properties.Resources.btnbg01;
            Image topSelimage = Properties.Resources.btnbg02;

            btn.BackColor = SelColor;
            btnPage.ForeColor = topSelColor;
            btnPage.BackgroundImage = topSelimage;

            //处理未选中 的模块按钮、未显示的 分页按钮
            UMenuButton[] menuBtns = { 仓库模块按钮, 产品模块按钮, 温度模块按钮 };
            UPageButton[] pageBtns = { btnStorePage, btnSRegionPage, btnProductPage, btnProductInStorePage, 出库页面按钮, btnStoreTemperPage };

            foreach (UMenuButton b in menuBtns)
            {
                if (b.Name != btn.Name)
                {
                    b.BackColor = unSelColor;

                }
            }

            foreach (UPageButton b in pageBtns)
            {
                if (btnPage.Name != b.Name)
                {
                    b.ForeColor = topUnSelColor;
                    b.BackgroundImage = topUnSelimage;

                }
            }

        }





        private void 仓库模块按钮_Click(object sender, EventArgs e)
        {
            ShowStoreManage();//显示仓库管理页面


        }



        private void 产品模块按钮_Click(object sender, EventArgs e)
        {
            SetModuleBtnBackClolor(2);
            product.产品信息页面 fProductList = new product.产品信息页面();
            panelPage.AddPanelForm(fProductList);
        }

        private void 温度模块按钮_Click(object sender, EventArgs e)
        {
            SetModuleBtnBackClolor(3);
            storeTemper.FrmStoreRegionTemperatureList fStoreTempertList = new storeTemper.FrmStoreRegionTemperatureList();
            panelPage.AddPanelForm(fStoreTempertList);
        }

        private void panelPage_AutoSizeChanged(object sender, EventArgs e)
        {

        }



        private void btnClosePage_Click(object sender, EventArgs e)
        {
            //if (panelPage.Controls.Count>0)
            // {
            panelPage.Controls.Clear();//关闭panelpage中的页面
            Color 未选中的显示的颜色 = Color.FromArgb(45, 50, 116);//默认或没有显示页面时

            btnStorePage.ForeColor = 未选中的显示的颜色;//默认或没有显示页面时
            btnStorePage.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;

            btnProductInStorePage.ForeColor = 未选中的显示的颜色;//默认或没有显示页面时
            btnProductInStorePage.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;


            出库页面按钮.ForeColor = 未选中的显示的颜色;//默认或没有显示页面时
            出库页面按钮.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;

            btnSRegionPage.ForeColor = 未选中的显示的颜色;//默认或没有显示页面时
            btnSRegionPage.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;

            btnProductPage.ForeColor = 未选中的显示的颜色;//默认或没有显示页面时
            btnProductPage.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;


            btnStoreTemperPage.ForeColor = 未选中的显示的颜色;//默认或没有显示页面时
            btnStoreTemperPage.BackgroundImage = STMS.STMSApp.Properties.Resources.btnbg01;

            //  BackgroundImage nosec= STMS.STMSApp.Properties.Resources.btnbg01;

            //}
            //else
            //{
            //    MsgBoxHelper.MsgErrorShow("关闭提示", "没有可关闭的");
            //}


        }

        private void 仓库管理页面按钮_Click(object sender, EventArgs e)
        {

            store.FrmStoreList new仓库管理页面 = new store.FrmStoreList();
            panelPage.AddPanelForm(new仓库管理页面);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(panelPage.Size.Width.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(fff.Name);
        }

        private void panelPage_Paint(object sender, PaintEventArgs e)
        {

        }

        private void 主页面窗体_SizeChanged(object sender, EventArgs e)
        {

            panelPage.Update();

        }
    }


}
