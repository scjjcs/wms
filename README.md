# WMS

## 简介

仓库管理系统练习代码

视频地址：https://www.bilibili.com/video/BV1q34y1W7hL/?=3&spm_id_from=pageDriver&vd_source=aade28408008fe0ffee5016cb40b8051

实现效果：

![输入图片说明](README/0.png)

## 数据库配置步骤

1. 登录数据库，选择数据库--安全性--登录名--sa--设定新密码--状态--启用

![输入图片说明](README/sql1.png)

选择数据库--右键属性--安全性--服务器身份验证--选择SQL Server和 Windows 身份验证模式

![输入图片说明](README/sql3.png)

2. 选择数据库，右键重启数据库

3. 修改数据库存储位置，创建示例数据库

![输入图片说明](README/SQL.png)

修改为自己要存放的地址，连接上数据库后直接点击执行，创建示例数据库。

4. 数据库连接前改为自己的数据库名称

![输入图片说明](README/APP.config.png)

## 知识点

30. 创建各模块管理页面

仓库（辅助）：仓库管理 仓库分区

产品 (辅助）：产品管理 产品人库

仓库温度管理（核心）分区：存储产品

31. 封装Panel中显示Form页方法

方法中包含的内容：

TopLevel：不能顶级窗体false

FormBorderStyle None

DockFill

AutoScroll：true

WindowStateMaximum

最后将窗体添加到Panel中：Panel.Controls.Add(frm):

32. 左边模块切换功能实现

### 1. SQL建表

建立6张表:

![输入图片说明](README/1.png)

### 2. 项目结构

![输入图片说明](README/2.png)

UI：Winform应用程序/WPF/Web

BLL：业务逻辑--业务逻辑处理，连接UI与DAL的桥梁

DAL：数据访问--处理数据（数据库）

此外，还有：

DbUtility：数据访问通用类库

Models：实体模型层 （类：一系列的属性：表中的列名）

 DModels--数据表对应实体类

 UIModels--UI层，BLL层的实体类

 VModels--视图对应的实体类

Common： 辅助工具层

Communicate：通信类库

### 3. 添加新项目--Windows 窗体应用（.NET Framework）

### 4. 项目间的互相引用

![输入图片说明](README/3.png)

### 5. 根据数据库表创建类

### 6. 表、列、主键特性创建

特性：用于在运行时传递程序中各种元素（比如类、方法、结构、枚举、组件等）的行为信息的声明性标签

反射技术

添加三个特性：

TableAttribute：用来映射数据库表名

ColumnAttribute：映射列名

PrimaryKeyAttribute：标注主键列名


## Common层

### AttributeHelper.cs

扩展方法：静态类中的静态方法，this修饰--作用于哪个类型,向现有类型添加方法，像调用实例一样使用

GetTName(this Type type)

GetColName(this PropertyInfo property)

string GetPrimary(this Type type)

IsPrimary(this Type type,PropertyInfo property)

IsIncrement(this Type type)

### PropertyHelper.cs

PropertyHelper详解--获取指定类型的指定列名的属性数组

作用类型：实体类、生成Insert Update Select 拼接列名，以“，”隔开的字符串

PropertyInfo[] GetTypeProperties<T>(string cols)

返回全部属性数组：cols-->""

返回部分列的属性数组：cols-->"UserId,username,userpwd"

用到了泛型、反射、扩展方法、字符串的转换

### DbConvert

![输入图片说明](README/DbConvert.png)

## Base

### BaseDAL 所有对象增删改查

BaseDAL T //其中T表示各种类型，泛型。泛型+反射,实现一个通用类覆盖多个DAL操作类

添加 单个、批量（导入、多个信息同时入库）

修改 单个、批量

删除 按编号（单个或批量）、条件（删除方式）---》条件SQL

添加 ：---Add

----------AddList

修改：---Update  以主键为条件/条件修改（两个修改方法）

----------UpdateList

删除：多表联级删除---判断--是否允许删除--条件满足？---允许，删除；不允许，不能删除。

---------Delete 条件删除 单个以ID为条件删除

---------DeleteList 批量删除

### BQuery----通用查询类

查询 条件获取单个实体/多个、判断存在性（例如用户登录）、DataTable/DataSet----BQuery----通用查询类

视图---》 查询，不能修改数据（增加、修改、删除）BQuery

总结：增加、修改、删除功能放在BaseDAL类中；查询功能放在BQuery类中。

# UI

## 自定义控件

![UPanel](README/16%E5%9C%86%E8%A7%92Panel.png)

![圆角按钮](README/19%E5%9C%86%E8%A7%92%E6%8C%89%E9%92%AE.png)

# 步骤

## 添加登录页面

1. 在Properties中添加系统级资源，整个项目都能访问，图标、logo等

在运行项目下 -- Properties --双击Resources.resx--添加资源--添加现有文件

2. MaximizeBox 设为 False ，最大化按钮设为不可用

3. 登录按钮中的判断：

接收页面输入

检查信息非空检查

登录过程检查用户的存在性 ui--bll--dal,检查（select）--核心

检查结果，作处理：存在成功，显主面；不存在，中断

4. 登录过程

DAL中的登录方法：账号、密码检查存在---查询

BLL中的登录法：账号、密码，调用DAL中法，返回结果，当userld>0 结果为 true

登录按钮处理程序中，调用B中登录方法，根据结果，进行相应的处理

## 主页面布局

UMenuButton制作：

1. 外观布局

2. 添加扩展属性：图标、按钮文本

3. 重写OnMouseClick/OnMouseEnter/OnMouseLeave

4. 整个控件的Click事件处理

UPageButton制作--UserControl(Label)

1. 默认设置ForeColor BackgroundImageLayout BtnText Font

2. 重写OnClick ForeColor的变化 LabelClick与用户控件的Click关联

## 顶部分页按钮关联与切换实现 （10月10日）

方法1：各分页按钮注册事件，在每个事件中，实例化相关页面，打开，显示到Panel中----不推荐

方法2：将各个页面的Form类名与分页按钮关联，文件夹.Form名称，设置为分页按钮的Tag

## 仓库管理页面布局

1、页面在pannel中显示是不需要边框的，所以设置页面的属性：FormBorderStyle值为None

  页面尺寸：Size属性设为：996，585

最小尺寸：MinimumSize：996，585

背景颜色：BackColor：53，61，133（深蓝色）

![欢迎加入技术交流群](README/wxq.png)

## 43

![输入图片说明](README/43.png)

## 45 仓库批量删除功能

删除按钮：只针对逻辑删除（单个或多个）批量删除

## 47 仓库信息页功能方法封装

1.方法：增：清空，改：根据编号获取仓库信息：GetStore(int storeld)

仓库名称已存在（检名称存在性）ExistsStore(string storeName,storeNo)

保存：入库：AddStore(storelnfo)或AddStoreReturnld(storelnfo)

更新：UpdateStore(storelnfo：包括编号）

2.方法实现：DAL--BLL调用DAL的方法，并进行相应的处理

## 48 仓库信息页功能

页面加载处理、清空、返回响应

## 53 仓库分区页面查询方法封装

条件仓库分区FindsRegionList(storeldstateld.keywords.isDel

--视图ViewStoreReqionInfos--联合仓库信与分区信息

1.建视图

2.创建视图实体类

3.封装查询方法DA

4.在StoreRegionBLL中定义调用法：调用第3步中封装的方法

### Tips 小技巧

1. 自动换行：工具--选项--文本编辑器-- C#--常规--勾选 自动换行

2.字体变更：工具--选项--环境--字体和颜色--选择 文本编辑器--选择Consolas字体

3.离线编译生成跳过还原NuGet包：工具--选项--NuGet包管理--取消勾选 在Visual Studio中生成期间自动检查缺少的程序包

4.代码片段：快速生成函数代码：ctor      ；     快速生成变量：prop

5.代码格式自动整理快捷键：先按CTRL+K 释放后再按CTRL+D 

# 报错记录

1. 操作系统返回错误21（设备未准备好。）

解决办法: **重新启动SQL Server，错误将消失。** 
