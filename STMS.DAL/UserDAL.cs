﻿using STMS.Models.DModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STMS.DAL
{
    public class UserDAL:BaseDAL<UserInfos>    
    {
       /// <summary>
       /// 用户登录
       /// </summary>
       /// <param name="uName"></param>
       /// <param name="uPwd"></param>
       /// <returns></returns>       
        public int LoginSystem(string uName, string uPwd)
        {
            string strWhere = "UserName=@userName and UserPwd=@userPwd and UserState=1 and IsDeleted=0";//参数化的SQL语句
            SqlParameter[] paras =
            {
            new SqlParameter("@userName",uName),
            new SqlParameter("@userPwd",uName)
            };
            UserInfos user = GetModel(strWhere, "UserId", paras);
            if (user != null)
            {
                return user.UserId;
            }
            else
            {
                return 0;
            }
        }
    }
}
