﻿using STMS.Models.VModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace STMS.DAL
{
    public class ViewStoreRegionDAL:BQuery<ViewStoreRegionInfo>
    {
        /// <summary>
        /// 条件查询出库分区列表，条件拼接
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="stateId"></param>
        /// <param name="keywords"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public List<ViewStoreRegionInfo> FindStoreRegionList(int storeId,int stateId,string keywords,int isDeleted)
        {
            List<ViewStoreRegionInfo> list = new List<ViewStoreRegionInfo>();
            string cols = CreateSql.GetColsString<ViewStoreRegionInfo>("StoreID,TemperState,Remark,IsDeleted");//不需要显示的字段
            string strWhere = $"IsDeleted={isDeleted}";
            if (storeId > 0)
                strWhere += $" and StoreId={storeId}";
            if (stateId > -1)
                strWhere += $" and TemperState={stateId}";
            if (string.IsNullOrEmpty(keywords))//有参数
            {
                strWhere += " and (SRegionNo like @keywords or SRegionName like @Keywords)";
                SqlParameter para = new SqlParameter("@keywords",$"%{keywords}%");
                list = GetModelList(strWhere, cols, para);//拼接
            }
            else
            {
                list = GetModelList(strWhere, cols);//无参数，不拼接
            }       
            return list;
        }
    }
}
